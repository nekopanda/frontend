package com.nekopanda.web.frontend.service;

import com.nekopanda.web.frontend.dto.Product;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Component
public class PromoServiceFallback implements PromoService {
    @Override
    public Iterable<Product> findAllProduct() {
        return new ArrayList<>();
    }

    @Override
    public Map<String, Object> hostinfo() {
        Map<String,Object> result = new HashMap<>();
        result.put("hostname","localhost");
        result.put("ip","127.0.0.1");
        result.put("port","-1");
        return result;
    }
}
