package com.nekopanda.web.frontend.service;

import com.nekopanda.web.frontend.dto.Product;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Map;

@FeignClient(value = "promo",fallback = PromoServiceFallback.class)
public interface PromoService {
    @GetMapping("/product/")
    Iterable<Product> findAllProduct();

    @GetMapping("/hostinfo")
    Map<String,Object> hostinfo();
}
