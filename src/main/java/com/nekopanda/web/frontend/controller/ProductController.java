package com.nekopanda.web.frontend.controller;

import com.nekopanda.web.frontend.service.PromoService;
import com.netflix.discovery.converters.Auto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ProductController {

    @Autowired
    private PromoService promoService;

    @GetMapping("/product/list")
    public ModelMap listOfProduct(){
        return new ModelMap().addAttribute("products",promoService.findAllProduct());
    }
}
