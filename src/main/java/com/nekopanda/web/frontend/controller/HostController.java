package com.nekopanda.web.frontend.controller;

import com.nekopanda.web.frontend.service.PromoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HostController {
    @Autowired
    private PromoService promoService;

    @GetMapping("/host/info")
    public ModelMap hostinfo(){
        return new ModelMap().addAttribute("hostinfo",promoService.hostinfo());
    }
}
